package com.devcamp.j03.javabasic.s50;
import java.util.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import com.devcamp.j03.javabasic.s10.Person;
import java.util.Arrays;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
public class Order2 {
    int id; // id của order
    String customerName; // tên khách hàng
    long price;// tổng giá tiền
    Date orderDate; // ngày thực hiện order
    boolean confirm; // đã xác nhận hay chưa?
    String[] items; // danh sách mặt hàng đã mua
    Person buyer;// người mua, là một object thuộc class Person
    // khởi Order2 1 tham số customerName
    public Order2(String customerName){
        this.id = 1;
        this.customerName = customerName;
        this.price = 120000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[]{"Book","Pen", "Rule"};
        this.buyer = new Person("Han") ;
    }
    //khởi tạo với tất cả tham số
    public Order2(int id,String customerName,long price, Date orderDate, boolean confirm, String[] items,String buyer){
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = confirm;
        this.items = items;
        this.buyer = new Person(buyer) ;
    }
    //khởi tạo với không tham số
    public Order2(){
        this("2");
    }
    //Khởi tạo với 3 tham số
    public Order2(int id,String customerName,long price){
        this(id, customerName, price,new Date(),true, new String[]{"Trung","Ca", "Rau","Dau"},"Tam");
    }
    @Override
    public String toString(){
        // Định dạng tiêu chuẩn Việt Nam
        Locale.setDefault(new Locale("vi","VN"));
        //Định dạng ngày tháng
        String pattern = "dd-MMMM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter defaultTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        //Định dạng cho giá tiền
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        //Return (trả ra) chuỗi (string)
        return
            "Order[id= "+ id
            +" ,customerName= " + customerName
            +" ,price= " + usNumberFormat.format(price)
            +" ,orderDate= " + defaultTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
            +" ,confirm= " + confirm
            +" ,items= " + Arrays.toString(items)
            +" ,buyer= " + this.buyer.toString();
    }
}
