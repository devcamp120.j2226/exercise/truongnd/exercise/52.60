import java.util.ArrayList;
import java.util.Date;
import com.devcamp.j03.javabasic.s50.Order2;
public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Order2> arrayList = new ArrayList<>();
        //Khởi tạo person với các tham số khác nhau
        Order2 order0 = new Order2();
        Order2 order1 = new Order2("Lan");
        Order2 order2 = new Order2(3,"Long",80000);
        Order2 order3 = new Order2(4,"Nam",75000,new Date(),false,new String[]{"Hop Mau","Tay","Giay Mau"},"Linh");
        //thêm object person vào danh sách
        arrayList.add(order0);
        arrayList.add(order1);
        arrayList.add(order2);
        arrayList.add(order3);
        //in ra màn hình
        for (Order2 order : arrayList){
            System.out.println(order.toString());
        }
    }
}
